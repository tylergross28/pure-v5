import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/main-pages/Home'
import WhatIsCbd from '@/main-pages/WhatIsCbd'
import EpilepsyOutreach from '@/main-pages/EpilepsyOutreach'
import TheSmokeroom from '@/main-pages/TheSmokeroom'
import ShopCbd from '@/main-pages/ShopCbd'
import PrivacyPolicy from '@/policies/PrivacyPolicy'
import ReturnPolicy from '@/policies/ReturnPolicy'
import ShippingPolicy from '@/policies/ShippingPolicy'
import TermsAndConditions from '@/policies/TermsAndConditions'
import CategoryList from '@/detail-pages/CategoryList'
import ProductDetail from '@/detail-pages/ProductDetail'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/what-is-cbd',
      name: 'what-is-cbd',
      component: WhatIsCbd
    },
    {
      path: '/epilepsy-outreach',
      name: 'epilepsy-outreach',
      component: EpilepsyOutreach
    },
    {
      path: '/the-smokeroom',
      name: 'the-smokeroom',
      component: TheSmokeroom
    },
    {
      path: '/shop-cbd',
      name: 'shop-cbd',
      component: ShopCbd
    },
    {
      path: '/policies/privacy-policy',
      name: 'privacy-policy',
      component: PrivacyPolicy
    },
    {
      path: '/policies/return-policy',
      name: 'return-policy',
      component: ReturnPolicy
    },
    {
      path: '/policies/shipping-policy',
      name: 'shipping-policy',
      component: ShippingPolicy
    },
    {
      path: '/policies/terms-and-conditions',
      name: 'terms-and-conditions',
      component: TermsAndConditions
    },
    {
      path: '/category/:catSlug',
      name: 'category-list',
      component: CategoryList
    },
    {
      path: '/product/:productSlug',
      component: ProductDetail,
      name: 'product-detail'
    }
  ]
})
