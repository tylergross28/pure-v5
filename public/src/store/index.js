import Vue from 'vue'
import VueResource from 'vue-resource'
import Vuex from 'vuex'
import router from '../router'

Vue.use(Vuex)
Vue.use(VueResource)
Vue.use(router)

Vue.http.options.root = 'https://pure-backend.appspot.com/wp-json'
Vue.http.headers.common['Authorization'] = 'Basic Y2tfYjM4ZDRmZjYzYWY5ZmI5MDE4ZWVlYjhmN2YwZTM2MzAyYTE3MGQ1NDpjc18xMWIxNjlmMjk0NzI3MTY4ZWJmMWJlYTNmMzI0ZGVhOTE5YmQ5ZDYx'

export default new Vuex.Store({
  state: {
    categories: [],
    products: []
  },
  mutations: {
    computeCategories () {
      Vue.http
        .get('wc/v2/products/categories')
        .then(response => {
          let vm = this
          response.body.forEach(element => {
            if (element.display === 'default') {
              vm.state.categories.push(element)
            }
          })
        })
        .catch(error => {
          console.log('Error getting categories data: ', error)
        })
    },
    computeProducts () {
      Vue.http
        .get('wc/v2/products?per_page=30')
        .then(response => {
          let vm = this
          response.body.forEach(element => {
            if (element.status === 'publish') {
              vm.state.products.push(element)
            }
          })
        })
        .catch(error => {
          console.log('Error getting products data: ', error)
        })
    }
  },
  actions: {
    computeCategories: context => {
      context.commit('computeCategories')
    },
    computeProducts: context => {
      context.commit('computeProducts')
    }
  }
})
